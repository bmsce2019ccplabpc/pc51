#include<stdio.h>
struct time
{
    int h;
    int m;
};
typedef struct time Time;
Time input_time()
{
    Time t;
    printf("Ente the time:\n");
    scanf("%d%d",&t.h,&t.m);
    return t;
}   
int compute(Time t)
{
    int total_time;
    total_time=t.h*60+t.m;
    return total_time;
}
void output(Time t,int total_time)
{
    printf("The total time of %d and %d is %d",t.h,t.m,total_time);
}
int main()
{
    Time t;
    int total_time;
    t=input_time();
    total_time=compute(t);
    output(t,total_time);
    return 0;
}
